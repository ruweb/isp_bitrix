#!/usr/bin/perl

use CGI qw/:standard/;

$Q = new CGI;
$func = $Q->param(func);
$elid = $Q->param(elid);

print "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<doc>";

if ($func eq "bitrix.confirm") {
    print "<elid>$elid</elid>\n";
    print "<dir>/www/$elid</dir>\n";
    $short=$elid;
    my $maxlen=16-length($ENV{REMOTE_USER})-1;
    my ($dom, $ext) = split(/\.([^\.]+)$/, $elid);
    $dom =~s|[\.\-]||g;
    if (length($dom)<=$maxlen && length($dom.$ext)>$maxlen) {
	$short=$dom;
    } else {
	$short=substr($dom.$ext.(length($dom.$ext)<$maxlen-1?"_bitrix":""),0,$maxlen);
    }
    print "<dbname>$ENV{REMOTE_USER}_${short}</dbname>\n";
    print "<dbuser>$ENV{REMOTE_USER}_${short}</dbuser>\n";
    $pass = join ('', map {('a'..'z','A'..'Z','0'..'9','_')[rand 63]} 0..9);
    print "<dbpass>$pass</dbpass>\n";
    print "<cleardb>yes</cleardb>\n";
    print "<cleardir>yes</cleardir>\n";
} elsif ( $func eq "bitrix") {
    my $mgrctl="/usr/local/ispmgr/sbin/mgrctl";
    if (!$Q->param(extop)){
        print "<redirect>top.location=\"http://".$Q->param(domain).$Q->param(path)."/bitrixsetup.php\"</redirect>";
    } else {
	my %p;
	$cleardb= $Q->param(cleardb) eq "on";
	$cleardir= $Q->param(cleardir) eq "on";
	$p{dbname}=$Q->param(dbname);
	$p{dbuser}=$Q->param(dbuser);
	$p{dbpass}=$Q->param(dbpass);
	$p{domain}=$Q->param(elid);
	$p{dir}=$Q->param(dir);
	$p{user}=$ENV{REMOTE_USER};
	foreach (@p) {
	    if ( /"^[\w\d\_\\]+$"/ ) { print "<error>Wrong characters used in fields input! Only alpha-numeric and underscore characters allowed.</error></doc>\n"; exit };
	}
	$p{dir} =~ s|^/||;
	my ($pwName, $pwCode, $pwUid, $pwGid, $pwQuota, $pwComment, $pwGcos, $pwHome, $pwLogprog) = getpwnam($p{user});
	my $mgrctl="/usr/local/ispmgr/sbin/mgrctl";
	print "<domain>$p{domain}</domain>\n";
	$path="$pwHome/$p{dir}";
	$path=~ s|^$pwHome/www/$p{domain}||;
	print "<domain>$p{domain}</domain>\n";
	print "<path>$path</path>\n";
	unless ( $pwUid && $pwGid && $pwHome ) { print "<error>Error 001</error></doc>\n"; exit}
	unless (-d "$pwHome/$p{dir}") { print "<error>Directory not exist</error></doc>\n"; exit}
	unless ( (stat "$pwHome/$p{dir}")[4]==$pwUid ) { print "<error>Directory access error</error></doc>\n"; exit}
	print "<domain>$p{domain}</domain>\n";
	$path="$pwHome/$p{dir}";
	$path=~ s|^$pwHome/www/$p{domain}||;
	print "<path>$path</path>\n";
	if (open(PS,"$mgrctl db su=$p{user} |")) {
	    while ( <PS> ) {
		$dbexist=1 if $_=~/\sname=$p{dbname}\s/;
		$duexist=1 if $_=~/\sdbuser=$p{dbuser}(\s|\,)/;
	    }
	} else { print "<error>Failed to get db list!</error></doc>\n"; exit}
	if ($dbexist && $cleardb) {
	    if (qx($mgrctl db.delete elid=\"MySQL->$p{dbname}\" su=$p{user}) !~ /^OK\Z/) {print "<error>Error deleting DB $mgrctl db.delete elid=\"MySQL->$p{dbname}\" su=$p{user}</error></doc>\n"; exit};
	}
	$cmd="$mgrctl db.edit name=$p{dbname} dbtype=MySQL owner=$p{user} dbencoding=cp1251 dbusername=$p{dbuser} dbpassword=$p{dbpass} sok=ok su=$p{user}".($duexist && !$cleardb?" dbuser=$p{dbuser}":"");
	if (qx($cmd) !~ /^OK\Z/) {print "<error>Error creating DB</error></doc>\n"; exit};
	if ($duexist && !$dbexist) {
	    $cmd="$mgrctl db.users.edit dbpassword=$p{dbpass} elid=$p{dbuser} plid=\"MySQL->$p{dbname}\" dbusername=$p{dbuser} select_priv=on insert_priv=on update_priv=on delete_priv=on create_priv=on drop_priv=on references_priv=on index_priv=on alter_priv=on lock_priv=on sok=ok su=$p{user}";
	    if (qx($cmd) !~ /^OK\Z/) {print "<error>Error modifying DB user</error></doc>\n"; exit};
	}
	$<=$pwUid; $(=$pwGid; $)=$pwGid; $>=$pwUid;
	if ($cleardir) { system("rm -rf $pwHome/$p{dir}/* $pwHome/$p{dir}/.*"); }
	unless ( -d "$pwHome/$p{dir}/bitrix" || mkdir "$pwHome/$p{dir}/bitrix") { print "<error>Error creating directory $pwHome/$p{dir}/bitrix: $!</error></doc>\n"; exit}
	unless ( -d "$pwHome/$p{dir}/bitrix/php_interface" || mkdir "$pwHome/$p{dir}/bitrix/php_interface") {print "<error>Error creating directory $pwHome/$p{dir}/bitrix/php_interface: $!</error></doc>\n"; exit}

	unless (open(FILE, ">$pwHome/$p{dir}/bitrix/php_interface/dbconn.php")) { print "<error>Error 003</error></doc>\n"; exit}
	print FILE <<EOF;
<?
define("SHORT_INSTALL", true);
define("SHORT_INSTALL_CHECK", true);
define("DBPersistent", false);
\$DBType = "mysql";
\$DBHost = "localhost";
\$DBLogin = "$p{dbuser}";
\$DBPassword = "$p{dbpass}";
\$DBName = "$p{dbname}";
\$DBDebug = false;
\$DBDebugToFile = false;

\@set_time_limit(60);

define("DELAY_DB_CONNECT", true);
define("CACHED_b_file", 3600);
define("CACHED_b_file_bucket_size", 10);
define("CACHED_b_lang", 3600);
define("CACHED_b_option", 3600);
define("CACHED_b_lang_domain", 3600);
define("CACHED_b_site_template", 3600);
define("CACHED_b_event", 3600);
define("CACHED_b_agent", 3660);
define("CACHED_menu", 3600);

define("BX_FILE_PERMISSIONS", 0644);
define("BX_DIR_PERMISSIONS", 0755);
\@umask(~BX_DIR_PERMISSIONS);
\@ini_set("memory_limit", "512M");
define("BX_DISABLE_INDEX_PAGE", true);
?>
EOF
	chmod(0600,"$pwHome/$p{dir}/bitrix/php_interface/dbconn.php");
	close(FILE);

	unless (open(FILE, ">$pwHome/$p{dir}/bitrix/php_interface/after_connect.php")) { print "<error>Error 003</error></doc>\n"; exit}
	print FILE <<EOF;
<?
\$DB->Query("SET NAMES 'cp1251'");
?>
EOF
	chmod(0600,"$pwHome/$p{dir}/bitrix/php_interface/after_connect.php");
	close(FILE);

	unless (open(FILE, ">$pwHome/$p{dir}/bitrix/php_interface/after_connect_d7.php")) { print "<error>Error 003</error></doc>\n"; exit}
	print FILE <<EOF;
<?
\$connection = \\Bitrix\\Main\\Application::getConnection();
\$connection->queryExecute("SET NAMES 'cp1251'");
?>
EOF
	chmod(0600,"$pwHome/$p{dir}/bitrix/php_interface/after_connect_d7.php");
	close(FILE);

	unless (system("/usr/bin/fetch -qo $pwHome/$p{dir}/bitrixsetup.php http://www.1c-bitrix.ru/download/scripts/bitrixsetup.php") == 0){ print "<error>Error 004</error></doc>\n"; exit}

	print "<d>Подготовка к установке <b>1С-Битрикс</b> выполнена. Нажмите кнопку \"Ok\" чтобы перейти на страницу загрузки: http://$p{domain}$path/bitrixsetup.php</d>";
    }
}

print "</doc>";
exit 0;
